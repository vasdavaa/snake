# Snake játék 

A játékos egy kigyót irányít, egy elhatárolt síkon. A kigyó a játék elején 6 egység hosszal rendelkezik. A pályaterületeten mindig egy alma található, ezt kell a játékosnak megennie azáltal, hogy nekimegy a kígyóval. Minden egyes alma felvétele, megnöveli a kígyó hosszát, 1 egységgel. A játékos veszít, hogyha a falba vagy saját magába ütközik. A kígyó kormányzása a balra, fel, le és jobbra nyilakkal történik.

